package com.lotos4u.tests.chess.boards;

import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;

public class MicroChessBoard {
	protected int xSize;
	protected int ySize;
	protected int nPoints;
	protected int nPieces;
	protected int nCompact;
	protected char[] boardPiecesNames;
	protected char[] compactPieces;
	public static long equalsCounter;
	public static long hashCounter;
	
	public MicroChessBoard(ChessBoardLight input) {
		xSize = input.xSize;
		ySize = input.ySize;
		nPoints = xSize*ySize;
		nPieces = input.nPieces;
		nCompact = nPieces*2;
		
		boardPiecesNames = ArrayUtils.clone(input.boardPiecesNames);
		
	}
	protected void updateCompactPieces(char[] names) {
		int index = 0;
		for (byte i = 0; i < nPoints; i++) {
			if (names[i] != AbstractChessBoard.NONAME) {
				compactPieces[index] = names[i];
				compactPieces[index+nPieces] = (char)i;
				index++;
			}
		}
	}


	@Override
	public boolean equals(Object obj) {
		MicroChessBoard b = (MicroChessBoard) obj;
		return Arrays.equals(boardPiecesNames, b.boardPiecesNames);
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(boardPiecesNames);
    }
}
