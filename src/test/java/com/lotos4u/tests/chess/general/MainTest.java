package com.lotos4u.tests.chess.general;

import org.junit.Assert;
import org.junit.Test;

import com.lotos4u.tests.chess.boards.ChessBoardLight;

public class MainTest {

	@Test
	public void testBoard7x7() {
		Log.out("\n\n********************** Test testBoard7x7 **********************\n");
		char[] pcs1 = new char[]{
				ChessBoardLight.QUEEN,
				ChessBoardLight.QUEEN,
				ChessBoardLight.BISHOP,
				ChessBoardLight.BISHOP,
				ChessBoardLight.KING, 
				ChessBoardLight.KING,
				ChessBoardLight.KNIGHT 
			};
		ChessBoardLight boardLight = new ChessBoardLight(7, 7, pcs1);		
		ChessBoardLight.sortPoints = true;
		long startLight = System.currentTimeMillis();

		int n = boardLight.getArrangementVariants(false, false, false, false, false);
		long endLight = System.currentTimeMillis();
		
		System.out.println("N = " + n + ", Time = " + (endLight - startLight) + " ms");
		
		Assert.assertEquals(3063828, n);
	
	}		
}
